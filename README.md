docker-mj-sif
==============

This document describes the requirement allowing to easily run the components for the RTI MJ SIF using [docker](https://www.docker.com/) containers.

Prerequisites
-------------

* [install Docker Compose](https://docs.docker.com/compose/ "Documentation") 


How to have MJ SIF running in one command?
--------------


Set the following environment variables in the .env file

 * MONGO_USER 
 * MONGO_PASS
 * MONGO_DB
 * MONGO_HOST
 * POSTGRES_DB
 * POSTGRES_HOST
 * POSTGRES_PASS





Start a terminal, then run:

```
git clone git@bitbucket.org:adam704a/docker-mj-sif.git
cd docker-mj-sif
docker-compose up -d
```

Once the container is up, open url http://docker.host.ip


Docker images
--------------
Docker images can be found at [Docker Hub](https://registry.hub.docker.com/repos/researchtriangle/ "Docker Hub")


Bugs, new requests or contribution
--------------
Please submit bugs, gripes and feature requests at https://bitbucket.org/adam704a/docker-mj-sif/issues

Any other questions contact Adam Preston on Twitter at @adam704a, email at apreston@rti.org
