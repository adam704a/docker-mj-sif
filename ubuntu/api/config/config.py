import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    DEBUG = True

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(Config):
    DEBUG     = True
    MONGO_URI = None
    MONGO_DB  = "aggregation_database_name"
    POSTGRES_URL = "postgresql://postgres:"+os.environ['POSTGRES_PASS']+"@"+os.environ['POSTGRES_HOST']+"/"+os.environ['POSTGRES_DB']

class ProductionConfig(Config):
    DEBUG = False
    MONGO_URI = "mongodb://"+os.environ['MONGO_USER']+":"+os.environ['MONGO_PASS']+"@"+os.environ['MONGO_HOST']+"/admin"
    MONGO_DB  = os.environ['MONGO_DB']
    POSTGRES_URL = "postgresql://postgres:"+os.environ['POSTGRES_PASS']+"@"+os.environ['POSTGRES_HOST']+"/"+os.environ['POSTGRES_DB']

config = {
    'development' : DevelopmentConfig,
    'production'  : ProductionConfig,
    'default'     : DevelopmentConfig
}